﻿
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace IntranetPortal.Models
{
    [Table("Items")]
    public class Item
    {
        public int ItemId { get; set; }
        public string Title { get; set; }
        public string Author { get; set; }
        public string ISBN { get; set; }
        public decimal Pages { get; set; }
        public string Publisher { get; set; }

        public string Status { get; set; }

        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}",
               ApplyFormatInEditMode = true)]
        public DateTime? PublishedDate { get; set; }

        public string Description { get; set; }
        public int LibraryId { get; set; }


    }
}

