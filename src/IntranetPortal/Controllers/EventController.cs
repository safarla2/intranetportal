using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using IntranetPortal.Models;
using Microsoft.EntityFrameworkCore;

namespace IntranetPortal.Controllers
{
    public class EventController : Controller
    {

        AppDbContext db = new AppDbContext();

        public IActionResult Index()
        {
            return View();
        }



        [HttpGet]
        public IActionResult AddEvent()
        {
            return View();

        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult AddEvent([Bind("Title, eTime, Attend, Description")]Event _event)
        {
            {


                if (ModelState.IsValid)
                {
                    db.Events.Add(_event);
                    db.SaveChanges();
                }
                return View(_event);


            }
        }


        public IActionResult WillAttend([Bind("Attend")]Event _event)
        {
            if (ModelState.IsValid)
            {
                db.Entry(_event).State = EntityState.Modified;
                db.SaveChanges();

            }

            return View(_event);

        }
    }
}