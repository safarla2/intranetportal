﻿using System;

namespace IntranetPortal.Models
{
    public class ItemViewModel
    {
        public int ItemId { get; set; }
        public string Title { get; set; }
        public string Author { get; set; }
        public string ISBN { get; set; }
        public decimal Pages { get; set; }
        public string Publisher { get; set; }
        public string Status { get; set; }
        public DateTime? PublishedDate { get; set; }
        public string Description { get; set; }
        public int LibraryId { get; set; }
        public virtual Library Libraries { get; set; }

    }
}
