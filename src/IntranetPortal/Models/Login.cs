﻿using System.ComponentModel.DataAnnotations;

namespace IntranetPortal.Models
{
    public class Login
    {
        [Required]
        [EmailAddress]
        public string EmailAddress { get; set; }

        [Required]
        [DataType(DataType.Password)]
        public string Password { get; set; }
    }
}


