﻿using System.ComponentModel.DataAnnotations;

namespace IntranetPortal.Models
{
    public class Register
        {
            [Required]
            [EmailAddress]
            [Display(Name = "Email")]
            public string Email { get; set; }

            [Required]
            [StringLength(100, ErrorMessage = "Agilli password sec", MinimumLength = 6)]
            [DataType(DataType.Password)]
            [Display(Name = "Password")]
            public string Password { get; set; }


            [DataType(DataType.Password)]
            [Display(Name = "Confirm Password")]
            [Compare("Password", ErrorMessage = "duz yaz qaqashim")]
            public string ConfirmPassword { get; set; }

        }
    }


