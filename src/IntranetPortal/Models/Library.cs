﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace IntranetPortal.Models
{
    [Table("Libraries")]
    public class Library
    {
        public int LibraryId { get; set; }
        [Required]
        public string LibraryName { get; set; }
        [Required]
        public string LibraryType { get; set; }
        public virtual ICollection<Item> Items { get; set; }

    }
}


