﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace IntranetPortal.Models
{

    [Table("Events")]
    public class Event
    {
        public int EventId { get; set; }
        public string Title { get; set; }

        public DateTime? eTime { get; set; }
        public bool Attend { get; set; }
        public string Description { get; set; }
    }
}
