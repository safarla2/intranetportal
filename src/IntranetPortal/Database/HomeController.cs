﻿using IntranetPortal.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using System.Linq;
using System;
using IntranetPortal.Helpers;
using Microsoft.EntityFrameworkCore;


// For more information on enabling MVC for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace IntranetPortal.Controllers
{
    public class HomeController : Controller
    {

        AppDbContext DbContext = new AppDbContext();

        [HttpGet]
        public IActionResult Index()
        {

            var model = DbContext.Libraries.Select(a => new Library
            {
                LibraryId = a.LibraryId,
                LibraryName = a.LibraryName,
                Items = a.Items
            });
              
            return View(model.ToList());
        }

        [HttpGet]
        public IActionResult Item()
        {

            ViewBag.LibraryId = new SelectList(DbContext.Libraries, "LibraryId", "LibraryName");
            return View();

        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Item(Item existing, ItemViewModel item)
        {

            if (ModelState.IsValid)
            {
                existing.ItemId = item.ItemId;
                existing.Title = item.Title;
                existing.Pages = item.Pages;
                existing.ISBN = item.ISBN;
                existing.PublishedDate = item.PublishedDate;
                existing.Status = item.Status;
                existing.Author = item.Author;
                existing.LibraryId = item.LibraryId;
                existing.Publisher = item.Publisher;
                DbContext.Items.Add(existing);
                DbContext.SaveChanges();
            }
            ViewBag.LibraryId = new SelectList(DbContext.Libraries, "LibraryId", "LibraryName", item.LibraryId);
            return View(item);
            
            
        }


        [HttpGet]
        public  IActionResult Library()
        {
            return View();
        }

        [HttpPost]
        public IActionResult Library([Bind("LibraryName,LibraryType")]Library library)
        {
            if (ModelState.IsValid)
            {
                DbContext.Libraries.Add(library);
                DbContext.SaveChanges();
            }
            return View(library);
        }


        [HttpGet]
        public IActionResult LibDetails(int tofiqId, string searchString)
        {
  
            var model = from m
                        in DbContext.Items
                        select m;

            if (String.IsNullOrEmpty(searchString ) && tofiqId != 0)

            { 
                model = from m
                            in DbContext.Items
                            where m.LibraryId == tofiqId
                            select m;

            }
            
            
            if (!String.IsNullOrEmpty(searchString))
                {
                    model = model.Where(s => s.Title.Contains(searchString));
                }  

            return View(model.ToList());
        }


        [HttpGet]
        public IActionResult All_Items()
        {
            var model = from m in DbContext.Items select m;
            return View(model.ToList());
        }
       
  
       [HttpGet]
       public IActionResult ItemPage(int pageId)
        {
            var model = from m in DbContext.Items where m.ItemId == pageId select m;
            return View(model.ToList());
           
        }


        [HttpPost]
        public IActionResult TakeItem(ItemViewModel item)
        {


            Item existing = DbContext.Items.FirstOrDefault(m => m.ItemId == item.ItemId);

            existing.Status = "Unavailable";
            DbContext.Entry(existing).State = EntityState.Modified;
            DbContext.SaveChanges();

            return RedirectToAction(nameof(HomeController.All_Items), "Home");


        }

    }
}
