﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace IntranetPortal.Models
{
    public class AppUser : IdentityUser { }
    public class AppDbContext : IdentityDbContext<AppUser>
    {
        protected override void OnModelCreating(ModelBuilder builder)
        {           
            base.OnModelCreating(builder);

        }
        protected override void OnConfiguring(DbContextOptionsBuilder builder)
        {
            builder.UseSqlServer(@"Data Source=192.168.2.156;Initial Catalog=eLibraryGTS;Integrated Security=False;User ID=sa;Password=1AWJ7fOTrcXvMj8K;Connect Timeout=15;Encrypt=False;TrustServerCertificate=True;ApplicationIntent=ReadWrite;MultiSubnetFailover=False");
        }

        public DbSet<Item> Items { get; set; }
        public DbSet<Library> Libraries { get; set; }

        public DbSet<Event> Events { get; set; }
    }
}


